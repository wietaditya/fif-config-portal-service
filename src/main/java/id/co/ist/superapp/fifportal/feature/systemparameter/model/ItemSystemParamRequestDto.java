package id.co.ist.superapp.fifportal.feature.systemparameter.model;

import lombok.Data;

@Data
public class ItemSystemParamRequestDto {
    private Long id;
}
