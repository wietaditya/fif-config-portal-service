package id.co.ist.superapp.fifportal.feature.systemparameter.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ItemSystemParamResponseDto {
    Object systemParamDto;
}
