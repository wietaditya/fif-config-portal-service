package id.co.ist.superapp.fifportal.constant;

public class ResponseCode {
    public static final String SOURCE_SYSTEM = "FIF-PORTAL";
    public static final String ERROR_GENERAL = "5000";
    public static final String INTEGRATION_FAILED = "5002";
    public static final String PARAM_INVALID_VALUES = "4000";
    public static final String CONFIRM_PASSWORD_NOT_SAME_WITH_NEW_PASSWORD = "4001";
    public static final String AUTH_USER_LOCKED = "4002";
    public static final String AUTH_USER_BLOCKED = "4003";
    public static final String DATA_NOT_FOUND = "4004";
    public static final String AUTH_ACCESS_DENIED = "4005";
    public static final String SESSION_INVALID = "4006";
    public static final String DATA_ALREADY_EXIST = "4007";
    public static final String USERNAME_PASSWORD_INVALID = "4008";
    public static final String NEW_PASSWORD_SAME_WITH_OLD_PASSWORD = "4009";
    public static final String USER_ALREADY_BLOCKED = "4010";
    public static final String USER_ALREADY_UNBLOCKED = "4011";

}
