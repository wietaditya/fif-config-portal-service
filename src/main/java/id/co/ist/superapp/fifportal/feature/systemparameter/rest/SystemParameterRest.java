package id.co.ist.superapp.fifportal.feature.systemparameter.rest;

import id.co.ist.superapp.basedomain.dto.parameter.systemparameter.UpdateSystemParameterReqDto;
import id.co.ist.superapp.basedomain.model.SystemParameter;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.swagger.ApiPageable;
import id.co.ist.superapp.fifportal.feature.systemparameter.model.*;
import id.co.ist.superapp.fifportal.feature.systemparameter.service.CreateSystemParameterService;
import id.co.ist.superapp.fifportal.feature.systemparameter.service.DeleteSystemParameterService;
import id.co.ist.superapp.fifportal.feature.systemparameter.service.SystemParameterService;
import id.co.ist.superapp.fifportal.feature.systemparameter.service.UpdateSystemParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@RequestMapping(value = "/systemparameter")
public class SystemParameterRest {

    @Autowired
    private SystemParameterService systemParameterService;

    @Autowired
    private CreateSystemParameterService createSystemParameterService;

    @Autowired
    private UpdateSystemParameterService updateSystemParameterService;

    @Autowired
    private DeleteSystemParameterService deleteSystemParameterService;

//    @PostMapping("/list")
//    @ResponseBody
//    @ApiPageable
//    public BasePageableResponseDto<List<SystemParamDto>> list(ListSystemParamRequestDto request, @ApiIgnore Pageable pageable) {
//        return systemParameterService.list(request, pageable);
//    }
//
    @PostMapping("/getById")
    @ResponseBody
    public ISTResponseDto<SystemParamDto> getById(@RequestBody ItemSystemParamRequestDto request) {
        return systemParameterService.getById(request);
    }

    @PostMapping("/create")
    @ResponseBody
    public ISTResponseDto<SystemParamDto> create(@RequestBody CreateSystemParamRequestDto request) {
        return createSystemParameterService.create(request);
    }

    @PostMapping("/update")
    @ResponseBody
    public ISTResponseDto<SystemParamDto> update(@RequestBody UpdateSystemParameterReqDto request) {
        return updateSystemParameterService.update(request);
    }

    @PostMapping("/delete")
    @ResponseBody
    public ISTResponseDto<SystemParamDto> delete(@RequestBody DeleteSystemParamRequestDto request) {
        return deleteSystemParameterService.delete(request);
    }

}
