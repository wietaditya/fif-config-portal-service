package id.co.ist.superapp.fifportal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"id.co.ist.superapp.*"})
@EnableJpaRepositories(basePackages = "id.co.ist.superapp.*")
@EntityScan(basePackages = {"id.co.ist.superapp.*"})
public class MainApplication {

	public static void main(String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}
}
