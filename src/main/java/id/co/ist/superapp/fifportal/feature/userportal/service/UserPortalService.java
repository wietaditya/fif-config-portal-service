package id.co.ist.superapp.fifportal.feature.userportal.service;

import id.co.ist.superapp.basedomain.dto.parameter.systemparameter.QuerySystemParameterResponseDto;
import id.co.ist.superapp.basedomain.model.UserPortal;
import id.co.ist.superapp.basedomain.repository.UserPortalRepository;
import id.co.ist.superapp.core.annotation.EnableQueryFilter;
import id.co.ist.superapp.core.dto.ISTPageableResponseDto;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.exception.ProcessException;
import id.co.ist.superapp.core.query.SearchSpecificationBuilder;
import id.co.ist.superapp.core.query.dto.CriteriaField;
import id.co.ist.superapp.core.util.ResponseUtil;
import id.co.ist.superapp.fifportal.constant.ResponseCode;
import id.co.ist.superapp.fifportal.feature.userportal.model.UserPortalResponseDto;
import id.co.ist.superapp.fifportal.manager.SchemaLoader;
import id.co.ist.superapp.utils.json.JsonConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserPortalService {

    @Autowired
    private UserPortalRepository userPortalRepository;

    public ISTPageableResponseDto<List<UserPortalResponseDto>> list(CriteriaField criteria) {
        var pageData = userPortalRepository.findAll(createSpecification(criteria), criteria.getPageable());

        List<UserPortalResponseDto> result = pageData.getContent().stream().map(
                x -> JsonConverterUtil.fromObject(x, UserPortalResponseDto.class)).collect(Collectors.toList());

        return ResponseUtil.success(new ISTPageableResponseDto<>(SchemaLoader.getContentInMap("user-portal.json"), pageData, result));
    }

    private Specification<UserPortal> createSpecification(CriteriaField criteria){
        SearchSpecificationBuilder<UserPortal> specificationBuilder = criteria.getSearchSpecificationBuilder(UserPortal.class.getAnnotation(EnableQueryFilter.class).columns());
        return specificationBuilder.build();
    }

    public ISTResponseDto<UserPortalResponseDto> getById(Long id) {
        log.info("Get user by id");
        var pageData = userPortalRepository.findById(id);

        if (pageData.isEmpty()) {
            log.info("Id user {} not found!", id);
            throw new ProcessException(ResponseCode.SOURCE_SYSTEM, ResponseCode.DATA_NOT_FOUND);
        }

        var result = JsonConverterUtil.fromObject(pageData.get(), UserPortalResponseDto.class);

        return ResponseUtil.success(result);
    }

}
