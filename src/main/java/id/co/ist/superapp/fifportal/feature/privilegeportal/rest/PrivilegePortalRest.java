package id.co.ist.superapp.fifportal.feature.privilegeportal.rest;

import id.co.ist.superapp.core.dto.ISTPageableResponseDto;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.query.dto.CriteriaField;
import id.co.ist.superapp.fifportal.feature.privilegeportal.model.CreatePrivilegeReqDto;
import id.co.ist.superapp.fifportal.feature.privilegeportal.model.GetPrivilegeByIdReqDto;
import id.co.ist.superapp.fifportal.feature.privilegeportal.model.PrivilegePortalResponseDto;
import id.co.ist.superapp.fifportal.feature.privilegeportal.model.UpdatePrivilegeReqDto;
import id.co.ist.superapp.fifportal.feature.privilegeportal.service.CreatePrivilegePortalService;
import id.co.ist.superapp.fifportal.feature.privilegeportal.service.DeletePrivilegePortalService;
import id.co.ist.superapp.fifportal.feature.privilegeportal.service.PrivilegePortalService;
import id.co.ist.superapp.fifportal.feature.privilegeportal.service.UpdatePrivilegePortalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/privilegePortal")
public class PrivilegePortalRest {

    @Autowired
    private PrivilegePortalService privilegePortalService;

    @Autowired
    private CreatePrivilegePortalService createPrivilegePortalService;

    @Autowired
    private UpdatePrivilegePortalService updatePrivilegePortalService;

    @Autowired
    private DeletePrivilegePortalService deletePrivilegePortalService;

    @PostMapping("/list")
    @ResponseBody
    public ISTPageableResponseDto<List<PrivilegePortalResponseDto>> list(@RequestBody CriteriaField criteria) {
        return privilegePortalService.list(criteria);
    }

    @PostMapping("/getById")
    @ResponseBody
    public ISTResponseDto<PrivilegePortalResponseDto> list(Long id) {
        return privilegePortalService.getById(id);
    }

    @PostMapping("/create")
    @ResponseBody
    public ISTResponseDto<PrivilegePortalResponseDto> create(@RequestBody CreatePrivilegeReqDto request) {
        return createPrivilegePortalService.create(request);
    }

    @PostMapping("/update")
    @ResponseBody
    public ISTResponseDto<PrivilegePortalResponseDto> update(@RequestBody UpdatePrivilegeReqDto request) {
        return updatePrivilegePortalService.update(request);
    }

    @PostMapping("/delete")
    @ResponseBody
    public ISTResponseDto<PrivilegePortalResponseDto> delete(Long id) {
        return deletePrivilegePortalService.delete(id);
    }

}
