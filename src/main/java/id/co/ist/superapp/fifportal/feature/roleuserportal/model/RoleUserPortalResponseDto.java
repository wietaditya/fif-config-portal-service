package id.co.ist.superapp.fifportal.feature.roleuserportal.model;

import id.co.ist.superapp.basedomain.model.PrivilegeUserPortal;
import id.co.ist.superapp.core.constant.UserPortalType;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Data
public class RoleUserPortalResponseDto implements Serializable {
    private Long id;
    private String name;
    private String description;
    private UserPortalType userPortalType;
    private Collection<PrivilegeUserPortal> privileges;
    private Boolean isDeleted;
}
