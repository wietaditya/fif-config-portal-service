package id.co.ist.superapp.fifportal.manager.rolemanager;

import lombok.Data;

@Data
public class RolePrivilegeManagerDto {
    private Long id;
    private String name;
    private String routeName;
    private String endpoint;
}
