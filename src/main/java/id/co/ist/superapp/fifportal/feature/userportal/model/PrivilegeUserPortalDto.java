package id.co.ist.superapp.fifportal.feature.userportal.model;

import lombok.Data;

@Data
public class PrivilegeUserPortalDto {

    private Long id;
    private String name;
    private String routeName;
    private String endpoint;
    private PrivilegeUserPortalDto parent;
}
