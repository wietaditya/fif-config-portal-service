package id.co.ist.superapp.fifportal.feature.roleuserportal.model;

import id.co.ist.superapp.core.constant.UserPortalType;
import lombok.Data;

import java.util.List;

@Data
public class CreateRoleUserReqDto {
    private String name;
    private String description;
    private UserPortalType userPortalType;
    private List<Long> privileges;
    private String createdBy;
}
