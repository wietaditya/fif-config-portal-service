package id.co.ist.superapp.fifportal.sys.postconstruct;

import id.co.ist.superapp.basedomain.model.UserPortal;
import id.co.ist.superapp.basedomain.repository.UserPortalRepository;
import id.co.ist.superapp.core.constant.UserPortalType;
import id.co.ist.superapp.core.constant.UserStatus;
import id.co.ist.superapp.core.util.AESCrypto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.Date;


@Component
@Slf4j
public class UserPortalInit {

    private final UserPortalRepository usrPortalRepo;

    public UserPortalInit(UserPortalRepository usrPortalRepo) {
        this.usrPortalRepo = usrPortalRepo;
    }

    @PostConstruct
    @Transactional
    void construct() {
        try {
            var usrPortalConfEnty = usrPortalRepo.findByUsernameEqualsIgnoreCaseAndUserPortalTypeEqualsAndIsDeletedEquals("adminportal", UserPortalType.FIF_PORTAL, false);
            if (usrPortalConfEnty.isEmpty()) {
                UserPortal usr = new UserPortal();
                usr.setUsername("adminportal");
                usr.setUserPortalType(UserPortalType.FIF_PORTAL);
                usr.setPassword(AESCrypto.encrypt("adminportal123"));
                usr.setIsDeleted(false);
                usr.setUserStatus(UserStatus.ACTIVE);
                usr.setCreatedDate(new Date());
                usr.setCreatedBy("PostConstruct");
                usrPortalRepo.save(usr);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
}
