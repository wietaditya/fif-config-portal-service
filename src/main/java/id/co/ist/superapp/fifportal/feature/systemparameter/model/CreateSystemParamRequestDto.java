package id.co.ist.superapp.fifportal.feature.systemparameter.model;

import lombok.Data;

@Data
public class CreateSystemParamRequestDto {
    private Long id;
    private String module;
    private String name;
    private String value;
    private String description;
    private Boolean isEncrypted;
    private String createdBy;
}
