package id.co.ist.superapp.fifportal.feature.roleuserportal.service;

import id.co.ist.superapp.basedomain.model.PrivilegeUserPortal;
import id.co.ist.superapp.basedomain.model.RoleUserPortal;
import id.co.ist.superapp.basedomain.repository.PrivilegeUserRepository;
import id.co.ist.superapp.basedomain.repository.RoleUserPortalRepository;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.exception.ProcessException;
import id.co.ist.superapp.core.util.ResponseUtil;
import id.co.ist.superapp.fifportal.constant.ResponseCode;
import id.co.ist.superapp.fifportal.feature.roleuserportal.model.CreateRoleUserReqDto;
import id.co.ist.superapp.fifportal.feature.roleuserportal.model.RoleUserPortalResponseDto;
import id.co.ist.superapp.fifportal.manager.rolemanager.RolePrivilegeManager;
import id.co.ist.superapp.utils.json.JsonConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Service
@Slf4j
public class CreateRoleUserPortalService {

    @Autowired
    private RoleUserPortalRepository roleUserPortalRepository;

    @Autowired
    private PrivilegeUserRepository privilegeUserRepository;

    @Autowired
    private RolePrivilegeManager rolePrivilegeManager;

    public ISTResponseDto<RoleUserPortalResponseDto> create(CreateRoleUserReqDto request) {
        log.info("Create new role user portal");
        var roleUser = roleUserPortalRepository.findByNameEquals(request.getName());

        if (roleUser.isPresent()) {
            log.info("Role name {} already exist", request.getName());
            throw new ProcessException(ResponseCode.SOURCE_SYSTEM, ResponseCode.DATA_ALREADY_EXIST);
        }

        RoleUserPortal roleUserPortal = new RoleUserPortal();

        Collection<PrivilegeUserPortal> privilegeUserPortals = new ArrayList<>();
        for (Long privilegeId : request.getPrivileges()) {
            PrivilegeUserPortal privilegeUserPortal = privilegeUserRepository.findByIdEquals(privilegeId);
            privilegeUserPortals.add(privilegeUserPortal);
        }

        roleUserPortal.setPrivileges(privilegeUserPortals);
        roleUserPortal.setName(request.getName());
        roleUserPortal.setUserPortalType(request.getUserPortalType());
        roleUserPortal.setDescription(request.getDescription());
        roleUserPortal.setIsDeleted(false);
        roleUserPortal.setCreatedDate(new Date());
        roleUserPortal.setCreatedBy(request.getCreatedBy());

        roleUserPortalRepository.save(roleUserPortal);

        rolePrivilegeManager.sync();
        var result = JsonConverterUtil.fromObject(roleUserPortal, RoleUserPortalResponseDto.class);

        return ResponseUtil.success(result);
    }
}
