package id.co.ist.superapp.fifportal.feature.roleuserportal.service;

import id.co.ist.superapp.basedomain.repository.RoleUserPortalRepository;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.exception.ProcessException;
import id.co.ist.superapp.core.util.ResponseUtil;
import id.co.ist.superapp.fifportal.constant.ResponseCode;
import id.co.ist.superapp.fifportal.feature.roleuserportal.model.RoleUserPortalResponseDto;
import id.co.ist.superapp.fifportal.manager.rolemanager.RolePrivilegeManager;
import id.co.ist.superapp.utils.json.JsonConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DeleteRoleUserPortalService {

    @Autowired
    private RoleUserPortalRepository roleUserPortalRepository;

    @Autowired
    private RolePrivilegeManager rolePrivilegeManager;


    public ISTResponseDto<RoleUserPortalResponseDto> delete(Long id) {
        log.info("Delete role user portal");
        var dataRoleUser = roleUserPortalRepository.findByIdEquals(id);

        if (dataRoleUser == null) {
            log.info("Id role {} not found!", id);
            throw new ProcessException(ResponseCode.SOURCE_SYSTEM, ResponseCode.DATA_NOT_FOUND);
        }

        dataRoleUser.setIsDeleted(true);

        roleUserPortalRepository.save(dataRoleUser);
        rolePrivilegeManager.sync();

        var result = JsonConverterUtil.fromObject(dataRoleUser, RoleUserPortalResponseDto.class);

        return ResponseUtil.success(result);
    }

}
