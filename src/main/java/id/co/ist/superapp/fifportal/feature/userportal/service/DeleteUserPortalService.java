package id.co.ist.superapp.fifportal.feature.userportal.service;

import id.co.ist.superapp.basedomain.model.UserPortal;
import id.co.ist.superapp.basedomain.repository.UserPortalRepository;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.exception.ProcessException;
import id.co.ist.superapp.core.util.ResponseUtil;
import id.co.ist.superapp.fifportal.constant.ResponseCode;
import id.co.ist.superapp.fifportal.feature.userportal.model.UserPortalResponseDto;
import id.co.ist.superapp.utils.json.JsonConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DeleteUserPortalService {

    @Autowired
    private UserPortalRepository userPortalRepository;

    public ISTResponseDto<UserPortalResponseDto> delete(Long id) {
        log.info("Delete user portal");

        var userExisting = userPortalRepository.findByIdEquals(id);

        if (userExisting.isEmpty()) {
            log.info("User id {} not found!", id);
            throw new ProcessException(ResponseCode.SOURCE_SYSTEM, ResponseCode.DATA_NOT_FOUND);
        }

        UserPortal userPortal = userExisting.get();
        userPortal.setIsDeleted(true);

        userPortalRepository.save(userPortal);
        var result = JsonConverterUtil.fromObject(userPortal, UserPortalResponseDto.class);

        return ResponseUtil.success(result);
    }

}
