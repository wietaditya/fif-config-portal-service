package id.co.ist.superapp.fifportal.feature.privilegeportal.model;

import id.co.ist.superapp.basedomain.model.PrivilegeUserPortal;
import id.co.ist.superapp.core.constant.UserPortalType;
import lombok.Data;

import java.io.Serializable;

@Data
public class PrivilegePortalResponseDto implements Serializable {
    private Long id;
    private String name;
    private String routeName;
    private String endpoint;
    private UserPortalType userPortalType;
    private PrivilegeUserPortal parent;
    private Boolean isDeleted;
}
