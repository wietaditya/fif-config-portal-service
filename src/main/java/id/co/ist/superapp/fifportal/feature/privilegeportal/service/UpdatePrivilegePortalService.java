package id.co.ist.superapp.fifportal.feature.privilegeportal.service;

import id.co.ist.superapp.basedomain.model.PrivilegeUserPortal;
import id.co.ist.superapp.basedomain.repository.PrivilegeUserRepository;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.exception.ProcessException;
import id.co.ist.superapp.core.util.ResponseUtil;
import id.co.ist.superapp.fifportal.constant.ResponseCode;
import id.co.ist.superapp.fifportal.feature.privilegeportal.model.PrivilegePortalResponseDto;
import id.co.ist.superapp.fifportal.feature.privilegeportal.model.UpdatePrivilegeReqDto;
import id.co.ist.superapp.fifportal.manager.rolemanager.RolePrivilegeManager;
import id.co.ist.superapp.utils.json.JsonConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
public class UpdatePrivilegePortalService {

    @Autowired
    private PrivilegeUserRepository privilegeUserRepository;

    @Autowired
    private RolePrivilegeManager rolePrivilegeManager;

    public ISTResponseDto<PrivilegePortalResponseDto> update(UpdatePrivilegeReqDto request) {
        log.info("Update privilege portal");

        var dataExisting = privilegeUserRepository.findByIdEquals(request.getId());

        if (dataExisting == null) {
            log.info("Id privilege {} not found!", request.getId());
            throw new ProcessException(ResponseCode.SOURCE_SYSTEM, ResponseCode.DATA_NOT_FOUND);
        }

        dataExisting.setName(request.getName());
        dataExisting.setRouteName(request.getRouteName());
        dataExisting.setEndpoint(request.getEndpoint());
        dataExisting.setUserPortalType(request.getUserType());
        dataExisting.setModifiedBy(request.getModifiedBy());
        dataExisting.setModifiedDate(new Date());

        if (request.getParent() != null) {
            PrivilegeUserPortal privilegeUserPortalParent = privilegeUserRepository.findByIdEquals(request.getParent());
            dataExisting.setParent(privilegeUserPortalParent);
        }

        privilegeUserRepository.save(dataExisting);
        rolePrivilegeManager.sync();

        var result = JsonConverterUtil.fromObject(dataExisting, PrivilegePortalResponseDto.class);

        return ResponseUtil.success(result);
    }
}
