package id.co.ist.superapp.fifportal.manager;

import id.co.ist.superapp.utils.json.JsonConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

@Slf4j
@Service
public class SchemaLoader {

	private static final String DIR = "schema";
	private Map<String, String> schemaList;
	private static SchemaLoader INSTANCE;

	@PostConstruct
	public void init(){
		log.info("Starting initialize schema...");
		INSTANCE = this;

		this.schemaList = new HashMap<>();

		var files = getInputStreamsFromClasspath();
		files.forEach((i) -> {
			try {
				var content = new String(i.getInputStream().readAllBytes(), StandardCharsets.UTF_8);
				this.schemaList.put(i.getFilename(), content);
			} catch (IOException e) {
				log.error("Error load schema "+i.getFilename(), e);
			}
		});
	}

	@PreDestroy
	public void destroy(){
		log.info("Destroy schema...");
		INSTANCE = null;
	}

	public static String getContent(String filename){
		return INSTANCE.schemaList.get(filename);
	}

	public static Map getContentInMap(String filename){
		return JsonConverterUtil.fromJsonString(INSTANCE.schemaList.get(filename), Map.class);
	}

	private Stream<Resource> getInputStreamsFromClasspath() {
		try {
			return Arrays.stream(new PathMatchingResourcePatternResolver().getResources("/" + DIR + "/**/*.json"))
					.filter(Resource::exists)
					.filter(Objects::nonNull);
		} catch (IOException e) {
			log.error("Failed to get definitions from directory {}", DIR, e);
			return Stream.of();
		}
	}


}
