package id.co.ist.superapp.fifportal.feature.privilegeportal.service;

import id.co.ist.superapp.basedomain.repository.PrivilegeUserRepository;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.exception.ProcessException;
import id.co.ist.superapp.core.util.ResponseUtil;
import id.co.ist.superapp.fifportal.constant.ResponseCode;
import id.co.ist.superapp.fifportal.feature.privilegeportal.model.PrivilegePortalResponseDto;
import id.co.ist.superapp.fifportal.manager.rolemanager.RolePrivilegeManager;
import id.co.ist.superapp.utils.json.JsonConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DeletePrivilegePortalService {

    @Autowired
    private PrivilegeUserRepository privilegeUserRepository;

    @Autowired
    private RolePrivilegeManager rolePrivilegeManager;


    public ISTResponseDto<PrivilegePortalResponseDto> delete(Long id) {
        log.info("Delete privilege portal");
        var dataPrivilege = privilegeUserRepository.findByIdEquals(id);

        if (dataPrivilege == null) {
            log.info("Id privilege {} not found!", id);
            throw new ProcessException(ResponseCode.SOURCE_SYSTEM, ResponseCode.DATA_NOT_FOUND);
        }

        dataPrivilege.setIsDeleted(true);

        privilegeUserRepository.save(dataPrivilege);
        rolePrivilegeManager.sync();

        var result = JsonConverterUtil.fromObject(dataPrivilege, PrivilegePortalResponseDto.class);

        return ResponseUtil.success(result);
    }

}
