package id.co.ist.superapp.fifportal.feature.systemparameter.model;

import lombok.Data;

import java.util.Date;

@Data
public class DeleteSystemParamRequestDto {
    private Long id;
    private String modifiedBy;
    private Date modifiedDate;
}
