package id.co.ist.superapp.fifportal.feature.privilegeportal.model;

import id.co.ist.superapp.core.constant.UserPortalType;
import lombok.Data;

@Data
public class CreatePrivilegeReqDto {
    private String name;
    private String routeName;
    private String endpoint;
    private UserPortalType userType;
    private Long parent;
    private String createdBy;
}
