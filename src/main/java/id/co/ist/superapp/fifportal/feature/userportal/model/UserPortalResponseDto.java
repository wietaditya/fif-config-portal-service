package id.co.ist.superapp.fifportal.feature.userportal.model;

import id.co.ist.superapp.basedomain.model.RoleUserPortal;
import id.co.ist.superapp.core.constant.UserPortalType;
import id.co.ist.superapp.core.constant.UserStatus;
import lombok.Data;

import java.util.Date;

@Data
public class UserPortalResponseDto {
    private Long id;
    private String username;
    private String password;
    private Date modifiedUsernameDate;
    private Integer numSuccessModifiedUsername;
    private Integer numFailedModifiedUsername;
    private Date dateLastSuccessLogin;
    private Date dateLastFailedLogin;
    private int numberFailedLogin;
    private Long deleteDateInLong=0L;
    private UserPortalType userPortalType;
    private String modifiedReason;
    private Date deletedDate;
    private Integer statusLogin;
    private RoleUserPortal role;
    private UserStatus userStatus;
    private String email;
    private String branchCode;
    private Long approvalId;
    private Boolean isDeleted;
}
