package id.co.ist.superapp.fifportal.feature.systemparameter.service;

import id.co.ist.superapp.basedomain.model.SystemParameter;
import id.co.ist.superapp.basedomain.repository.SystemParameterRepository;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.exception.ProcessException;
import id.co.ist.superapp.core.util.ResponseUtil;
import id.co.ist.superapp.fifportal.constant.ResponseCode;
import id.co.ist.superapp.fifportal.feature.systemparameter.model.CreateSystemParamRequestDto;
import id.co.ist.superapp.fifportal.feature.systemparameter.model.SystemParamDto;
import id.co.ist.superapp.utils.json.JsonConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
public class CreateSystemParameterService {

    @Autowired
    private SystemParameterRepository systemParameterRepository;

    public ISTResponseDto<SystemParamDto> create(CreateSystemParamRequestDto request) {
        log.info("Create system parameter");
        var dataFromDb = systemParameterRepository
                .findByModuleAndName(request.getModule(), request.getName());

        if (dataFromDb.isPresent()) {
            log.info("Data system parameter already exist");
            throw new ProcessException(ResponseCode.SOURCE_SYSTEM, ResponseCode.DATA_ALREADY_EXIST);
        }

        SystemParameter systemParameter = new SystemParameter();
        systemParameter.setModule(request.getModule());
        systemParameter.setName(request.getName());
        systemParameter.setIsEncrypted(request.getIsEncrypted());
        systemParameter.setValue(request.getValue());
        systemParameter.setDescription(request.getDescription());
        systemParameter.setCreatedBy(request.getCreatedBy());
        systemParameter.setCreatedDate(new Date());
        systemParameter.setIsDeleted(false);
        systemParameterRepository.save(systemParameter);

        var systemParam = JsonConverterUtil.fromObject(systemParameter, SystemParamDto.class);

        return ResponseUtil.success(systemParam);
    }

}
