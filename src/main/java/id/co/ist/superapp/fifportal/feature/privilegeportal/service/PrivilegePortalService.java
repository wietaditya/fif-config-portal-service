package id.co.ist.superapp.fifportal.feature.privilegeportal.service;

import id.co.ist.superapp.basedomain.model.PrivilegeUserPortal;
import id.co.ist.superapp.basedomain.repository.PrivilegeUserRepository;
import id.co.ist.superapp.core.annotation.EnableQueryFilter;
import id.co.ist.superapp.core.dto.ISTPageableResponseDto;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.exception.ProcessException;
import id.co.ist.superapp.core.query.SearchSpecificationBuilder;
import id.co.ist.superapp.core.query.dto.CriteriaField;
import id.co.ist.superapp.core.util.ResponseUtil;
import id.co.ist.superapp.fifportal.constant.ResponseCode;
import id.co.ist.superapp.fifportal.feature.privilegeportal.model.PrivilegePortalResponseDto;
import id.co.ist.superapp.fifportal.manager.SchemaLoader;
import id.co.ist.superapp.utils.json.JsonConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PrivilegePortalService {

    @Autowired
    private PrivilegeUserRepository privilegeUserRepository;

    public ISTPageableResponseDto<List<PrivilegePortalResponseDto>> list(CriteriaField criteria) {
        var pageData = privilegeUserRepository.findAll(createSpecification(criteria), criteria.getPageable());

        List<PrivilegePortalResponseDto> result = pageData.getContent().stream().map(
                x -> JsonConverterUtil.fromObject(x, PrivilegePortalResponseDto.class)).collect(Collectors.toList());

        return ResponseUtil.success(new ISTPageableResponseDto<>(SchemaLoader.getContentInMap("privilege-portal.json"), pageData, result));
    }

    private Specification<PrivilegeUserPortal> createSpecification(CriteriaField criteria){
        SearchSpecificationBuilder<PrivilegeUserPortal> specificationBuilder = criteria.getSearchSpecificationBuilder(PrivilegeUserPortal.class.getAnnotation(EnableQueryFilter.class).columns());
        return specificationBuilder.build();
    }

    public ISTResponseDto<PrivilegePortalResponseDto> getById(Long id) {
        log.info("Get privilege by id");
        var pageData = privilegeUserRepository.findById(id);

        if (pageData.isEmpty()) {
            log.info("Id privilege {} not found!", id);
            throw new ProcessException(ResponseCode.SOURCE_SYSTEM, ResponseCode.DATA_NOT_FOUND);
        }

        var result = JsonConverterUtil.fromObject(pageData.get(), PrivilegePortalResponseDto.class);

        return ResponseUtil.success(result);
    }

}
