package id.co.ist.superapp.fifportal.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MinioConfig {

    @Value("${minio.user:null}")
    String user;
    @Value("${minio.password:null}")
    String password;
    @Value("${minio.url:null}")
    String minioUrl;

//    @Bean
//    public MinioClient generateMinioClient() {
//        try {
//            MinioClient client = new MinioClient(minioUrl, user, password);
//            return client;
//        } catch (Exception e) {
//            throw new RuntimeException(e.getMessage());
//        }
//
//    }


}