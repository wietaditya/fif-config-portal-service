package id.co.ist.superapp.fifportal.feature.privilegeportal.model;

import lombok.Data;

@Data
public class GetPrivilegeByIdReqDto {
    private Long id;
}
