package id.co.ist.superapp.fifportal.manager.rolemanager;

import id.co.ist.superapp.basedomain.model.PrivilegeUserPortal;
import id.co.ist.superapp.basedomain.model.RoleUserPortal;
import id.co.ist.superapp.basedomain.repository.RoleUserPortalRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RolePrivilegeManager {

    private RoleUserPortalRepository roleUserPortalRepository;

    public RolePrivilegeManager(RoleUserPortalRepository roleUserPortalRepository) {
        this.roleUserPortalRepository = roleUserPortalRepository;
    }

    private static final Map<Long, List<RolePrivilegeManagerDto>> ROLE_PRIVILEGES = new HashMap<>();

    public static List<RolePrivilegeManagerDto> getPrivilegeByUserRole(Long roleId){
        return ROLE_PRIVILEGES.get(roleId);
    }

    @Transactional
    public void sync() {
        ROLE_PRIVILEGES.clear();
        List<RoleUserPortal> roleUserPortalList = (List<RoleUserPortal>) roleUserPortalRepository.findAll();
        for (RoleUserPortal roleUserPortal : roleUserPortalList) {
            List<RolePrivilegeManagerDto> rolePrivilegeManagerDtos = new ArrayList<>();
            for (PrivilegeUserPortal privilegeUserPortal : roleUserPortal.getPrivileges()) {
                RolePrivilegeManagerDto rolePrivilegeManagerDto = new RolePrivilegeManagerDto();
                rolePrivilegeManagerDto.setId(privilegeUserPortal.getId());
                rolePrivilegeManagerDto.setEndpoint(privilegeUserPortal.getEndpoint());
                rolePrivilegeManagerDto.setName(privilegeUserPortal.getName());
                rolePrivilegeManagerDto.setRouteName(privilegeUserPortal.getRouteName());
                rolePrivilegeManagerDtos.add(rolePrivilegeManagerDto);
            }
            ROLE_PRIVILEGES.put(roleUserPortal.getId(), rolePrivilegeManagerDtos);
        }
    }

    public static Boolean checkPrivilege(Long roleId, String privilege, String contextPath){
        List<RolePrivilegeManagerDto> rolePrivilegeManagerDtos =  ROLE_PRIVILEGES.get(roleId);
        return rolePrivilegeManagerDtos.stream().anyMatch(itemPrivilege -> contextPath.concat(itemPrivilege.getEndpoint()).equalsIgnoreCase(privilege));
    }

}
