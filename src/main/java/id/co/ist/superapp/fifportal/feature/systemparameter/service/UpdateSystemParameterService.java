package id.co.ist.superapp.fifportal.feature.systemparameter.service;

import id.co.ist.superapp.basedomain.dto.parameter.systemparameter.SystemParamaterDto;
import id.co.ist.superapp.basedomain.dto.parameter.systemparameter.UpdateSystemParameterReqDto;
import id.co.ist.superapp.basedomain.model.SystemParameter;
import id.co.ist.superapp.basedomain.repository.SystemParameterRepository;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.exception.ProcessException;
import id.co.ist.superapp.core.util.ResponseUtil;
import id.co.ist.superapp.fifportal.constant.ResponseCode;
import id.co.ist.superapp.fifportal.feature.systemparameter.model.SystemParamDto;
import id.co.ist.superapp.utils.json.JsonConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UpdateSystemParameterService {

    @Autowired
    private SystemParameterRepository systemParameterRepository;

    public ISTResponseDto<SystemParamDto> update(UpdateSystemParameterReqDto request) {
        log.info("Update system parameter");
        var sysParam = systemParameterRepository.findById(request.getId());

        if (sysParam.isEmpty()) {
            log.info("Data system parameter not found!");
            throw new ProcessException(ResponseCode.SOURCE_SYSTEM, ResponseCode.DATA_NOT_FOUND);
        }

        var sysParamByModuleAndName = systemParameterRepository
                .findByModuleAndName(request.getModule(), request.getName());

        if (sysParamByModuleAndName.isPresent()) {
            log.info("Data system parameter already exist");
            throw new ProcessException(ResponseCode.SOURCE_SYSTEM, ResponseCode.DATA_ALREADY_EXIST);
        }

        SystemParameter systemParameter = sysParam.get();
        systemParameter.setModule(request.getModule());
        systemParameter.setName(request.getName());
        systemParameter.setValue(request.getValue());
        systemParameter.setDescription(request.getDescription());
        systemParameter.setIsEncrypted(request.getIsEncrypted());
        systemParameter.setModifiedBy(request.getModifiedBy());
        systemParameter.setModifiedDate(request.getModifiedDate());
        systemParameterRepository.save(systemParameter);

        var result = JsonConverterUtil.fromObject(systemParameter, SystemParamDto.class);
        return ResponseUtil.success(result);
    }
}
