package id.co.ist.superapp.fifportal.feature.roleuserportal.service;

import id.co.ist.superapp.basedomain.model.RoleUserPortal;
import id.co.ist.superapp.basedomain.repository.RoleUserPortalRepository;
import id.co.ist.superapp.core.annotation.EnableQueryFilter;
import id.co.ist.superapp.core.dto.ISTPageableResponseDto;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.exception.ProcessException;
import id.co.ist.superapp.core.query.SearchSpecificationBuilder;
import id.co.ist.superapp.core.query.dto.CriteriaField;
import id.co.ist.superapp.core.util.ResponseUtil;
import id.co.ist.superapp.fifportal.constant.ResponseCode;
import id.co.ist.superapp.fifportal.feature.roleuserportal.model.RoleUserPortalResponseDto;
import id.co.ist.superapp.fifportal.manager.SchemaLoader;
import id.co.ist.superapp.utils.json.JsonConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RoleUserPortalService {

    @Autowired
    private RoleUserPortalRepository roleUserPortalRepository;

    public ISTPageableResponseDto<List<RoleUserPortalResponseDto>> list(CriteriaField criteria) {
        var pageData = roleUserPortalRepository.findAll(createSpecification(criteria), criteria.getPageable());

        List<RoleUserPortalResponseDto> result = pageData.getContent().stream().map(
                x -> JsonConverterUtil.fromObject(x, RoleUserPortalResponseDto.class)).collect(Collectors.toList());

        return ResponseUtil.success(new ISTPageableResponseDto<>(SchemaLoader.getContentInMap("role-user-portal.json"), pageData, result));
    }

    private Specification<RoleUserPortal> createSpecification(CriteriaField criteria){
        SearchSpecificationBuilder<RoleUserPortal> specificationBuilder = criteria.getSearchSpecificationBuilder(RoleUserPortal.class.getAnnotation(EnableQueryFilter.class).columns());
        return specificationBuilder.build();
    }

    public ISTResponseDto<RoleUserPortalResponseDto> getById(Long id) {
        log.info("Get role by id");
        var pageData = roleUserPortalRepository.findById(id);

        if (pageData.isEmpty()) {
            log.info("Id role {} not found!", id);
            throw new ProcessException(ResponseCode.SOURCE_SYSTEM, ResponseCode.DATA_NOT_FOUND);
        }

        var result = JsonConverterUtil.fromObject(pageData.get(), RoleUserPortalResponseDto.class);

        return ResponseUtil.success(result);
    }

}
