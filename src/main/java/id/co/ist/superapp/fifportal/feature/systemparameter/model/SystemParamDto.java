package id.co.ist.superapp.fifportal.feature.systemparameter.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SystemParamDto {
    private Long id;
    private String module;
    private String name;
    private String value;
    private String description;
    private Boolean isEncrypted;
    private String modifiedBy;
}
