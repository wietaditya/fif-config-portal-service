package id.co.ist.superapp.fifportal.feature.roleuserportal.service;

import id.co.ist.superapp.basedomain.model.PrivilegeUserPortal;
import id.co.ist.superapp.basedomain.repository.PrivilegeUserRepository;
import id.co.ist.superapp.basedomain.repository.RoleUserPortalRepository;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.exception.ProcessException;
import id.co.ist.superapp.core.util.ResponseUtil;
import id.co.ist.superapp.fifportal.constant.ResponseCode;
import id.co.ist.superapp.fifportal.feature.roleuserportal.model.RoleUserPortalResponseDto;
import id.co.ist.superapp.fifportal.feature.roleuserportal.model.UpdateRoleUserReqDto;
import id.co.ist.superapp.fifportal.manager.rolemanager.RolePrivilegeManager;
import id.co.ist.superapp.utils.json.JsonConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Service
@Slf4j
public class UpdateRoleUserPortalService {

    @Autowired
    private PrivilegeUserRepository privilegeUserRepository;

    @Autowired
    private RoleUserPortalRepository roleUserPortalRepository;

    @Autowired
    private RolePrivilegeManager rolePrivilegeManager;

    public ISTResponseDto<RoleUserPortalResponseDto> update(UpdateRoleUserReqDto request) {
        log.info("Update role user portal");

        var dataExisting = roleUserPortalRepository.findByIdEquals(request.getId());

        if (dataExisting == null) {
            log.info("Id role {} not found!", request.getId());
            throw new ProcessException(ResponseCode.SOURCE_SYSTEM, ResponseCode.DATA_NOT_FOUND);
        }

        Collection<PrivilegeUserPortal> privilegeUserPortals = new ArrayList<>();
        for (Long privilegeId : request.getPrivileges()) {
            PrivilegeUserPortal privilegeUserPortal = privilegeUserRepository.findByIdEquals(privilegeId);
            privilegeUserPortals.add(privilegeUserPortal);
        }

        dataExisting.setPrivileges(privilegeUserPortals);
        dataExisting.setName(request.getName());
        dataExisting.setDescription(request.getDescription());
        dataExisting.setUserPortalType(request.getUserPortalType());
        dataExisting.setModifiedBy(request.getModifiedBy());
        dataExisting.setModifiedDate(new Date());

        roleUserPortalRepository.save(dataExisting);
        rolePrivilegeManager.sync();

        var result = JsonConverterUtil.fromObject(dataExisting, RoleUserPortalResponseDto.class);

        return ResponseUtil.success(result);
    }
}
