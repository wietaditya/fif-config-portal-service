package id.co.ist.superapp.fifportal.feature.userportal.model;

import id.co.ist.superapp.core.constant.UserPortalType;
import id.co.ist.superapp.core.constant.UserStatus;
import lombok.Data;

import java.util.Date;

@Data
public class UpdateUserPortalRequestDto {
    private Long id;
    private String username;
    private String password;
    private String email;
    private Date modifiedUsernameDate;
    private Integer numSuccessModifiedUsername;
    private Integer numFailedModifiedUsername;
    private Date dateLastSuccessLogin;
    private Date dateLastFailedLogin;
    private int numberFailedLogin;
    private Long deleteDateInLong=0L;
    private UserPortalType userPortalType;
    private String modifiedReason;
    private Date deletedDate;
    private Integer statusLogin;
    private Long role;
    private String refreshToken;
    private Long expireRefreshToken;
    private String token;
    private UserStatus userStatus;
    private String modifiedBy;
}
