package id.co.ist.superapp.fifportal.feature.systemparameter.service;

import id.co.ist.superapp.basedomain.model.SystemParameter;
import id.co.ist.superapp.basedomain.repository.SystemParameterRepository;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.exception.ProcessException;
import id.co.ist.superapp.core.query.SearchCriteria;
import id.co.ist.superapp.core.query.SearchSpecificationBuilder;
import id.co.ist.superapp.core.util.ResponseUtil;
import id.co.ist.superapp.fifportal.constant.ResponseCode;
import id.co.ist.superapp.fifportal.feature.systemparameter.model.ItemSystemParamRequestDto;
import id.co.ist.superapp.fifportal.feature.systemparameter.model.ListSystemParamRequestDto;
import id.co.ist.superapp.fifportal.feature.systemparameter.model.SystemParamDto;
import id.co.ist.superapp.utils.json.JsonConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SystemParameterService {

    @Autowired
    private SystemParameterRepository systemParamRepository;
//
//    public BasePageableResponseDto<List<SystemParamDto>> list(ListSystemParamRequestDto request, Pageable pageable) {
//
//        var pageData = systemParamRepository.findAll(specification(request), pageable);
//
//        List<SystemParamDto> responseList = pageData.getContent().stream().map(
//                x -> JsonConverterUtil.fromObject(x, SystemParamDto.class)).collect(Collectors.toList());
//
//        BasePageableResponseDto<List<SystemParamDto>> ret = new BasePageableResponseDto<>();
//        ret.setData(pageData, responseList);
////        ResponseUtil.success(ret);
//        return ret;
//    }
//
    public ISTResponseDto<SystemParamDto> getById(ItemSystemParamRequestDto request) {

        var dataFromDb = systemParamRepository.findById(request.getId());

        if (dataFromDb.isEmpty()) {
            log.info("Data system parameter not found!");
            throw new ProcessException(ResponseCode.SOURCE_SYSTEM, ResponseCode.DATA_NOT_FOUND);
        }

        SystemParamDto sysParam = JsonConverterUtil.fromObject(dataFromDb.get(), SystemParamDto.class);
        var ret = ResponseUtil.success(sysParam);
        return ret;
    }
//
//    private Specification<SystemParameter> specification(ListSystemParamRequestDto request) {
//        SearchSpecificationBuilder<SystemParameter> andSpecification = new SearchSpecificationBuilder<>(false);
//        SearchSpecificationBuilder<SystemParameter> orSpecification = new SearchSpecificationBuilder<>(true);
//
//        if (request.getModule() != null) {
//            orSpecification = orSpecification.with(new SearchCriteria("module", request.getModule(), SearchCriteria.QueryOperator.contains));
//        }
//
//        if (request.getName() != null) {
//            orSpecification = orSpecification.with(new SearchCriteria("name", request.getName(), SearchCriteria.QueryOperator.contains));
//        }
//
//        andSpecification = andSpecification.with(new SearchCriteria("isDeleted", false, SearchCriteria.QueryOperator.eq));
//
//        return andSpecification.build().and(orSpecification.build());
//    }

}
