package id.co.ist.superapp.fifportal.feature.systemparameter.model;

import lombok.Data;

@Data
public class ListSystemParamRequestDto {
    private String name;
    private String module;
}
