package id.co.ist.superapp.fifportal.feature.roleuserportal.rest;

import id.co.ist.superapp.core.dto.ISTPageableResponseDto;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.query.dto.CriteriaField;
import id.co.ist.superapp.fifportal.feature.roleuserportal.model.CreateRoleUserReqDto;
import id.co.ist.superapp.fifportal.feature.roleuserportal.model.RoleUserPortalResponseDto;
import id.co.ist.superapp.fifportal.feature.roleuserportal.model.UpdateRoleUserReqDto;
import id.co.ist.superapp.fifportal.feature.roleuserportal.service.CreateRoleUserPortalService;
import id.co.ist.superapp.fifportal.feature.roleuserportal.service.DeleteRoleUserPortalService;
import id.co.ist.superapp.fifportal.feature.roleuserportal.service.RoleUserPortalService;
import id.co.ist.superapp.fifportal.feature.roleuserportal.service.UpdateRoleUserPortalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/roleUserPortal")
public class RoleUserPortalRest {

    @Autowired
    private RoleUserPortalService roleUserPortalService;

    @Autowired
    private CreateRoleUserPortalService createRoleUserPortalService;

    @Autowired
    private UpdateRoleUserPortalService updateRoleUserPortalService;

    @Autowired
    private DeleteRoleUserPortalService deleteRoleUserPortalService;

    @PostMapping("/list")
    @ResponseBody
    public ISTPageableResponseDto<List<RoleUserPortalResponseDto>> list(@RequestBody CriteriaField criteria) {
        return roleUserPortalService.list(criteria);
    }

    @PostMapping("/getById")
    @ResponseBody
    public ISTResponseDto<RoleUserPortalResponseDto> getById(Long id) {
        return roleUserPortalService.getById(id);
    }

    @PostMapping("/create")
    @ResponseBody
    public ISTResponseDto<RoleUserPortalResponseDto> create(@RequestBody CreateRoleUserReqDto request) {
        return createRoleUserPortalService.create(request);
    }

    @PostMapping("/update")
    @ResponseBody
    public ISTResponseDto<RoleUserPortalResponseDto> update(@RequestBody UpdateRoleUserReqDto request) {
        return updateRoleUserPortalService.update(request);
    }

    @PostMapping("/delete")
    @ResponseBody
    public ISTResponseDto<RoleUserPortalResponseDto> delete(Long id) {
        return deleteRoleUserPortalService.delete(id);
    }

}
