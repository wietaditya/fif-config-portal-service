package id.co.ist.superapp.fifportal.feature.systemparameter.service;

import id.co.ist.superapp.basedomain.model.SystemParameter;
import id.co.ist.superapp.basedomain.repository.SystemParameterRepository;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.exception.ProcessException;
import id.co.ist.superapp.core.util.ResponseUtil;
import id.co.ist.superapp.fifportal.constant.ResponseCode;
import id.co.ist.superapp.fifportal.feature.systemparameter.model.DeleteSystemParamRequestDto;
import id.co.ist.superapp.fifportal.feature.systemparameter.model.SystemParamDto;
import id.co.ist.superapp.utils.json.JsonConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DeleteSystemParameterService {
    
    @Autowired
    private SystemParameterRepository systemParameterRepository;

    public ISTResponseDto<SystemParamDto> delete(DeleteSystemParamRequestDto request) {
        log.info("Delete system parameter");

        var sysParam = systemParameterRepository.findById(request.getId());

        if (sysParam.isEmpty()) {
            log.info("Data system parameter not found!");
            throw new ProcessException(ResponseCode.SOURCE_SYSTEM, ResponseCode.DATA_NOT_FOUND);
        }

        SystemParameter systemParameter = sysParam.get();
        systemParameter.setIsDeleted(true);
        systemParameter.setModifiedBy(request.getModifiedBy());
        systemParameter.setModifiedDate(request.getModifiedDate());
        systemParameterRepository.save(systemParameter);

        var result = JsonConverterUtil.fromObject(systemParameter, SystemParamDto.class);
        return ResponseUtil.success(result);
    }
    
}
