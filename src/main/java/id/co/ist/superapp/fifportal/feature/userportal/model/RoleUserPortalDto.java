package id.co.ist.superapp.fifportal.feature.userportal.model;

import id.co.ist.superapp.core.constant.UserPortalType;
import lombok.Data;

import java.util.Collection;

@Data
public class RoleUserPortalDto {

    private Long id;
    private String name;
    private String description;
    private UserPortalType userPortalType;
    private Collection<PrivilegeUserPortalDto> privileges;
}
