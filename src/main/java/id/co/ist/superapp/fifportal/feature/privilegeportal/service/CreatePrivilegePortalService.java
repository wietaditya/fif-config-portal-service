package id.co.ist.superapp.fifportal.feature.privilegeportal.service;

import id.co.ist.superapp.basedomain.model.PrivilegeUserPortal;
import id.co.ist.superapp.basedomain.repository.PrivilegeUserRepository;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.util.ResponseUtil;
import id.co.ist.superapp.fifportal.feature.privilegeportal.model.CreatePrivilegeReqDto;
import id.co.ist.superapp.fifportal.feature.privilegeportal.model.PrivilegePortalResponseDto;
import id.co.ist.superapp.fifportal.manager.rolemanager.RolePrivilegeManager;
import id.co.ist.superapp.utils.json.JsonConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
public class CreatePrivilegePortalService {

    @Autowired
    private PrivilegeUserRepository privilegeUserRepository;

    @Autowired
    private RolePrivilegeManager rolePrivilegeManager;

    public ISTResponseDto<PrivilegePortalResponseDto> create(CreatePrivilegeReqDto request) {
        log.info("Create new privilege portal");
        PrivilegeUserPortal privilegeUserPortal = new PrivilegeUserPortal();
        if (request.getParent() != null) {
            PrivilegeUserPortal privilegeUserPortalParent = privilegeUserRepository.findByIdEqualsAndIsDeletedEquals(request.getParent(), false);
            privilegeUserPortal.setParent(privilegeUserPortalParent);
        }
        privilegeUserPortal.setName(request.getName());
        privilegeUserPortal.setRouteName(request.getRouteName());
        privilegeUserPortal.setEndpoint(request.getEndpoint());
        privilegeUserPortal.setUserPortalType(request.getUserType());
        privilegeUserPortal.setIsDeleted(false);
        privilegeUserPortal.setCreatedDate(new Date());
        privilegeUserPortal.setCreatedBy(request.getCreatedBy());

        privilegeUserRepository.save(privilegeUserPortal);

        rolePrivilegeManager.sync();
        var result = JsonConverterUtil.fromObject(privilegeUserPortal, PrivilegePortalResponseDto.class);

        return ResponseUtil.success(result);
    }
}
