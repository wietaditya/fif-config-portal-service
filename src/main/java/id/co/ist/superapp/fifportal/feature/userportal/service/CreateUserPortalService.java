package id.co.ist.superapp.fifportal.feature.userportal.service;

import id.co.ist.superapp.basedomain.model.UserPortal;
import id.co.ist.superapp.basedomain.repository.UserPortalRepository;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.exception.ProcessException;
import id.co.ist.superapp.core.util.AESCrypto;
import id.co.ist.superapp.core.util.ResponseUtil;
import id.co.ist.superapp.fifportal.constant.ResponseCode;
import id.co.ist.superapp.fifportal.feature.userportal.model.CreateUserPortalRequestDto;
import id.co.ist.superapp.fifportal.feature.userportal.model.UserPortalResponseDto;
import id.co.ist.superapp.utils.json.JsonConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
public class CreateUserPortalService {

    @Autowired
    private UserPortalRepository userPortalRepository;

    public ISTResponseDto<UserPortalResponseDto> create(CreateUserPortalRequestDto request) {
        log.info("Create new user portal");
        var userByUsername = userPortalRepository
                .findByUsernameEqualsIgnoreCaseAndUserPortalTypeEqualsAndIsDeletedEquals(
                        request.getUsername(), request.getUserPortalType(), false);

        if (userByUsername.isPresent()) {
            log.info("Username {} already exist", request.getUsername());
            throw new ProcessException(ResponseCode.SOURCE_SYSTEM, ResponseCode.DATA_ALREADY_EXIST);
        }

        var userByEmail = userPortalRepository
                .findByEmailEqualsIgnoreCaseAndUserPortalTypeEqualsAndIsDeletedEquals(
                        request.getUsername(), request.getUserPortalType(), false);

        if (userByEmail.isPresent()) {
            log.info("Email {} already exist", request.getEmail());
            throw new ProcessException(ResponseCode.SOURCE_SYSTEM, ResponseCode.DATA_ALREADY_EXIST);
        }

        UserPortal userPortal = new UserPortal();
        userPortal.setUsername(request.getUsername());
        userPortal.setPassword(AESCrypto.encrypt(request.getPassword()));
        userPortal.setEmail(request.getEmail().trim());
        userPortal.setModifiedUsernameDate(request.getModifiedUsernameDate());
        userPortal.setModifiedReason(request.getModifiedReason());
        userPortal.setNumberFailedLogin(request.getNumberFailedLogin());
        userPortal.setNumFailedModifiedUsername(request.getNumFailedModifiedUsername());
        userPortal.setNumSuccessModifiedUsername(request.getNumSuccessModifiedUsername());
        userPortal.setUserPortalType(request.getUserPortalType());
        userPortal.setUserStatus(request.getUserStatus());
        userPortal.setDateLastFailedLogin(request.getDateLastFailedLogin());
        userPortal.setDateLastSuccessLogin(request.getDateLastSuccessLogin());
        userPortal.setDeletedDate(request.getDeletedDate());
        userPortal.setDeleteDateInLong(request.getDeleteDateInLong());
        userPortal.setToken(request.getToken());
        userPortal.setRefreshToken(request.getRefreshToken());
        userPortal.setExpireRefreshToken(request.getExpireRefreshToken());
        userPortal.setStatusLogin(request.getStatusLogin());
        userPortal.setIsDeleted(false);
        userPortal.setCreatedDate(new Date());
        userPortal.setCreatedBy(request.getCreatedBy());

        userPortalRepository.save(userPortal);

        var result = JsonConverterUtil.fromObject(userPortal, UserPortalResponseDto.class);
        return ResponseUtil.success(result);
    }

}
