package id.co.ist.superapp.fifportal.feature.userportal.rest;

import id.co.ist.superapp.core.dto.ISTPageableResponseDto;
import id.co.ist.superapp.core.dto.ISTResponseDto;
import id.co.ist.superapp.core.query.dto.CriteriaField;
import id.co.ist.superapp.fifportal.feature.userportal.model.CreateUserPortalRequestDto;
import id.co.ist.superapp.fifportal.feature.userportal.model.UpdateUserPortalRequestDto;
import id.co.ist.superapp.fifportal.feature.userportal.model.UserPortalResponseDto;
import id.co.ist.superapp.fifportal.feature.userportal.service.CreateUserPortalService;
import id.co.ist.superapp.fifportal.feature.userportal.service.DeleteUserPortalService;
import id.co.ist.superapp.fifportal.feature.userportal.service.UpdateUserPortalService;
import id.co.ist.superapp.fifportal.feature.userportal.service.UserPortalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/userPortal")
public class UserPortalRest {

    @Autowired
    private UserPortalService userPortalService;

    @Autowired
    private CreateUserPortalService createUserPortalService;

    @Autowired
    private UpdateUserPortalService updateUserPortalService;

    @Autowired
    private DeleteUserPortalService deleteUserPortalService;

    @PostMapping("/list")
    @ResponseBody
    public ISTPageableResponseDto<List<UserPortalResponseDto>> list(@RequestBody CriteriaField criteria) {
        return userPortalService.list(criteria);
    }

    @PostMapping("/getById")
    @ResponseBody
    public ISTResponseDto<UserPortalResponseDto> getById(Long id) {
        return userPortalService.getById(id);
    }

    @PostMapping("/create")
    @ResponseBody
    public ISTResponseDto<UserPortalResponseDto> create(@RequestBody CreateUserPortalRequestDto request) {
        return createUserPortalService.create(request);
    }

    @PostMapping("/update")
    @ResponseBody
    public ISTResponseDto<UserPortalResponseDto> update(@RequestBody UpdateUserPortalRequestDto request) {
        return updateUserPortalService.update(request);
    }

    @PostMapping("/delete")
    @ResponseBody
    public ISTResponseDto<UserPortalResponseDto> delete(Long id) {
        return deleteUserPortalService.delete(id);
    }

}
