CREATE TABLE IF NOT EXISTS m_privilege_user_portal (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	route_name varchar(255) NULL,
	endpoint varchar(255) NULL,
	portal_type int4 NULL,
	parent_id int8 NULL,
    is_deleted int4 NOT NULL DEFAULT 0,
	created_by varchar(50) NULL,
    created_date timestamp NOT NULL,
    modification_by varchar(50) NULL,
    modification_date timestamp NULL,
	CONSTRAINT m_privilege_user_portal_pkey PRIMARY KEY (id),
	CONSTRAINT fk4tr2xn5tgv96nmyf02dhid0ng FOREIGN KEY (parent_id) REFERENCES m_privilege_user_portal(id)
);

CREATE TABLE IF NOT EXISTS m_role_user_portal (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	description varchar(255) NULL,
	role_type int4 NULL,
	is_deleted int4 NOT NULL DEFAULT 0,
    created_by varchar(50) NULL,
    created_date timestamp NOT NULL,
    modification_by varchar(50) NULL,
    modification_date timestamp NULL,
	CONSTRAINT m_role_user_portal_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS m_roles_privilege (
	role_id int8 NOT NULL,
	privilege_id int8 NOT NULL
);

ALTER TABLE m_roles_privilege ADD CONSTRAINT fk5uq25s4h72snu71k0575uw42x FOREIGN KEY (privilege_id) REFERENCES m_privilege_user_portal(id);
ALTER TABLE m_roles_privilege ADD CONSTRAINT fkqydroly3lg8ly7v307y5vmuax FOREIGN KEY (role_id) REFERENCES m_role_user_portal(id);

CREATE TABLE IF NOT EXISTS master_branch (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	address varchar(255) NULL,
	city varchar(255) NULL,
	branch_code varchar(255) NULL,
	is_deleted int4 NOT NULL DEFAULT 0,
    created_by varchar(50) NULL,
    created_date timestamp NOT NULL,
    modification_by varchar(50) NULL,
    modification_date timestamp NULL,
	CONSTRAINT master_branch_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS master_sequence_generator (
	id bigserial NOT NULL,
	seq_name varchar(20) NULL,
	seq_start_sequence int8 NULL,
	seq_max_sequence int8 NULL,
	seq_interval int8 NULL,
	seq_length int8 NULL,
	seq_pattern varchar(50) NULL,
	seq_reset varchar(50) NULL,
	seq_description varchar(255) NULL,
	is_deleted int4 NOT NULL DEFAULT 0,
    created_by varchar(50) NULL,
    created_date timestamp NOT NULL,
    modification_by varchar(50) NULL,
    modification_date timestamp NULL,
	modified_reason varchar(100) NULL,
	CONSTRAINT master_sequence_generator_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS master_user_portal (
	id bigserial NOT NULL,
	username varchar(255) NOT NULL,
	"password" varchar(255) NOT NULL,
	modified_username_date timestamp NULL,
	num_success_modified_username int4 NULL,
	num_failed_modified_username int4 NULL,
	date_last_success_login timestamp NULL,
	date_last_failed_login timestamp NULL,
	num_failed_login int4 NOT NULL,
	delete_date_inlong int8 NULL,
	user_type int4 NULL,
	modified_reason varchar(255) NULL,
	deleted_date timestamp NULL,
	status_login int4 NULL,
	"role" int8 NULL,
	"token" text NULL,
	refresh_token varchar(100) NULL,
	expire_refresh_token int8 NULL,
	user_status int4 NULL,
	email varchar(255) NULL,
	branch_code varchar(255) NULL,
	appproval_id int8 NULL,
	is_deleted int4 NOT NULL DEFAULT 0,
    created_by varchar(50) NULL,
    created_date timestamp NOT NULL,
    modification_by varchar(50) NULL,
    modification_date timestamp NULL,
	CONSTRAINT master_user_portal_pkey PRIMARY KEY (id),
	CONSTRAINT uk_93kysa1hbsqkomiw7xnty7lq6 UNIQUE (username)
);
ALTER TABLE master_user_portal ADD CONSTRAINT fk6rfou7p5pmfb1io4mh2sxocft FOREIGN KEY (role) REFERENCES m_role_user_portal(id);

CREATE TABLE IF NOT EXISTS t_approval_data_history (
	id bigserial NOT NULL,
	"key" varchar(255) NOT NULL,
	value text NOT NULL,
	old_value varchar(255) NULL,
	status varchar(255) NULL,
	"action" varchar(255) NULL,
	description varchar(500) NULL,
	item_id int8 NULL,
	is_deleted int4 NOT NULL DEFAULT 0,
    created_by varchar(50) NULL,
    created_date timestamp NOT NULL,
    modification_by varchar(50) NULL,
    modification_date timestamp NULL,
	CONSTRAINT t_approval_data_history_pkey PRIMARY KEY (id)
);
CREATE INDEX keyindex ON t_approval_data_history USING btree (key);
